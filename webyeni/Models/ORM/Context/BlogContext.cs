﻿using webyeni.Models.ORM.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace webyeni.Models.ORM.Context//buranın ismi
{
    public class BlogContext : DbContext
    {
        public BlogContext()
        {
            Database.Connection.ConnectionString = "Server=DESKTOP-IS6C6I1\\RTFSQL2014;Database=OnlineBlog; Trusted_connection=true;";
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<AdminUser> AdminUsers { get; set; }

        //tabloları set edecegim
        public DbSet<Category> Categories { get; set; }

        public DbSet<BlogPost> BlogPosts { get; set; }

        public DbSet<SiteMenü> SiteMenus { get; set; }

        public DbSet<BlogPostComment> BlogPostComment { get; set; }

        //ben burda sunu yaptım:
        //ilk once claslarımı olusturdum daha sonra db contextte  bu classların table oldugunu soyledı 
        //referansa entıty framework ekledım
        //daha sonra package manegar console dan olusturması ıcın enable-migrations yazdım kı veritabanınına gondersın bu bılgıyı
        //? isareti bos gecirelebilir demek bunu icin classın  

    }
}