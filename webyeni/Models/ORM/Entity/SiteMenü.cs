﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web.Models.ORM.Entity;

namespace webyeni.Models.ORM.Entity
{
    public class SiteMenü: BaseEntity
    {
        public string Name { get; set; }

        public string Url { get; set; }


        public string cssClass { get; set; }
    }
}