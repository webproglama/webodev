﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using web.Models.ORM.Entity;

namespace webyeni.Models.ORM.Entity
{
    public class BlogPost: BaseEntity  //seed tohum 
    {
        public string Title { get; set; }
        public string content { get; set; }

       
        public int CategoryID { get; set; }

        public string ImagePath { get; set; }//gelen resimleri kaydetmem icin bununda olması gerekiyo


        [ForeignKey("CategoryID")]
        public virtual Category category { get; set; }


    }
}