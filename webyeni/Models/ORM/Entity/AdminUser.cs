﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using web.Models.ORM.Entity;

namespace webyeni.Models.ORM.Entity
{
    public class AdminUser : BaseEntity
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        [Required]//bos gecirilemez
        [StringLength(30)]//email 30 karakter olsun dıyorum 
        public string EMail { get; set; }

        [Required]
        public string Password { get; set; }
    }
}