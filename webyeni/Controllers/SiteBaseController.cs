﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webyeni.Models.ORM.Context;

namespace webyeni.Controllers
{
    public class SiteBaseController : Controller
    {
        public BlogContext db;

       public SiteBaseController()
        {
            db = new BlogContext();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }
    }
}