﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webyeni.Models.ORM.Entity;
using webyeni.Models.VM;

namespace webyeni.Controllers
{
    public class SiteBlogController : SiteBaseController
    {

        public ActionResult Index(int id)
        {

            BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == id);//bunu ilk yakaladım
            SiteBlogPostVM model = new SiteBlogPostVM();

            model.Content = blogpost.content;
            model.PostImagePath = blogpost.ImagePath;
            model.Title = blogpost.Title;
            model.Category = blogpost.category.Name;

            return View(model);

        }
        [HttpPost]


        public ActionResult AddComment (CommentVM model)
        {
            BlogPostComment comment = new BlogPostComment();
            comment.Content = model.Content;
            comment.BlogPostID = model.BlogPostid;
            comment.Name = model.Name;
            comment.EMail = model.EMail;


            db.BlogPostComment.Add(comment);
            db.SaveChanges();

            return RedirectToAction("Index", "SiteBlog", new { id = model.BlogPostid });
        }
    }
}