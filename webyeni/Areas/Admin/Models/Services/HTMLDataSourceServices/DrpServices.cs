﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webyeni.Models.ORM.Context;

namespace webyeni.Areas.Admin.Models.Services.HTMLDataSourceServices
{
    public class DrpServices
    {//herzaman katagori doldurma islemi yapmamk icin bi kere buraya yapıp bıtırıcem

        public static IEnumerable<SelectListItem> getDrpCategories()
        {
            using (BlogContext db = new BlogContext())
            {
                IEnumerable<SelectListItem> drpCategories = db.Categories.Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.ID.ToString()
                }).ToList();

                return drpCategories;
            }

        }
    }
}