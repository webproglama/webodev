﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webyeni.Areas.Admin.Models.DTO
{
    public class BlogPostVM:BaseVM
    {
        public string Title { get; set; }//bitane baslık

        public String Content { get; set; }//ıcerik
        public string CategoryName { get; set; }

        [Display(Name="Ana Resim")]
        public HttpPostedFileBase  PostImage { get; set; }//resim ekleme islemi yapıyorum
        [Required]
        public int CategoryID { get; set; }

        public IEnumerable<SelectListItem> drpCategories { get; set; }//burda alacagı verileri yazdım
    }
}