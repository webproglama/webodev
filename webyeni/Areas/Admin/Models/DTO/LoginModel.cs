﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webyeni.Areas.Admin.Models.DTO
{
    public class LoginModel
    {
        [Required(ErrorMessage ="Lütfen Email adresinizi giriniz ")]//burda required mesaj vermemi saglayacak
        [EmailAddress (ErrorMessage ="Düzgün bir email adresi giriniz..:")]//düzgün email girmesse bu mesaj gecerli olacak

        public string EMail { get; set; }

        [Required(ErrorMessage ="Lütfen parola kısmını doldur ")]
        public string Password { get; set; }
    }
}