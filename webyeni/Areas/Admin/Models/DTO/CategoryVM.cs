﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webyeni.Areas.Admin.Models.DTO
{
    public class CategoryVM:BaseVM//ekranda neler olucagını burda belırlıcem
    {

        [Required(ErrorMessage ="Lütfen kategori Alanı giriniz..")]
        [Display(Name="Ad")]
        public string Name { get; set; }//dedimki bunlar gorulsun

        [Display(Name="Acıklama ")]
        public string  Description { get; set; }
    }
}