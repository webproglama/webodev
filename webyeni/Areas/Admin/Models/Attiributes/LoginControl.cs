﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webyeni.Areas.Admin.Models.DTO.Attiributes
{
    public class LoginControl:ActionFilterAttribute
    {//burda required gibi bı kontrol metodu yazdık 
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {                   //sorguya girdiği an diyoruz buraya
           if(!HttpContext.Current.User.Identity.IsAuthenticated)
            {//email tarafından girmiyosa
                filterContext.HttpContext.Response.Redirect("/Admin/Login/Index");
            }//admine girmek istediğinde logina atıcak beni
        }
    }
}