﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webyeni.Areas.Admin.Models.DTO;
using webyeni.Models.ORM.Entity;

namespace webyeni.Areas.Admin.Controllers
{
    public class CategoryController :BaseController//artık burdan kalıtım aldırıyoruz db otomatık koymus oluyoz
    {
        // GET: Admin/Category
        public ActionResult Index()
        {//burada listeleme islemi yapıcaksınız
            List<CategoryVM> model = db.Categories.Where(x => x.IsDeleted == false).OrderBy(x => x.AddDate).Select(x => new CategoryVM()
            {
                Name = x.Name,
                Description = x.Description,
                ID = x.ID
            }).ToList();
            return View(model);//burda cotogory vm sınıfımızdan list turunde istediğimiz sorgudan suzulen veriyi returnle index dosyasına fırlatmasını sagladık 
        }

        //tıkladıgında gelen ilk kısım
        public ActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]//verileri doldurup bastıgında buraya gelecek
        public ActionResult AddCategory(CategoryVM model)
        {if (ModelState.IsValid)//islemler dogruysa
            {
                Category category = new Category();//veritabanını cagır
                category.Name = model.Name;//model(veridekini) dbye at
                category.Description = model.Description;//model(veridekini) dbye at
                db.Categories.Add(category);
                db.SaveChanges();
                ViewBag.IslemDurum = 1;
                return View();
            }
            else
            {
                ViewBag.IslemDurum = 2;
                return View();//buraya uyarılarımız gelecek

            }
           
        }

    

     


        //bu goruntuleme bıde doldurdugunda yanı post ettıgındede yapıcam
        public ActionResult updateCategory(int id)
        {//once guncellenecek butun kategoriyi bulup verileri ekrana yazıyorum
            Category category = db.Categories.FirstOrDefault(x =>x.ID == id);
            CategoryVM model = new CategoryVM();
            model.Name = category.Name;
            model.Description = category.Description;

            return View(model);
        }

        [HttpPost]//doldurup basıldıgında
        public ActionResult UpdateCategory(CategoryVM model)
        {
            //guncellenecek kategori yakalanır ve yeni verilerle update yapılır 
            if (ModelState.IsValid)
            {
                Category category = db.Categories.FirstOrDefault(x => x.ID == model.ID);
                category.Name = model.Name;
                category.Description = model.Description;

                db.SaveChanges();
                ViewBag.IslemDurum = 1;
                return View();
            }
            else
            {
                ViewBag.IslemDurum = 1;
                return View();
            }


        }

        public JsonResult DeleteCategory(int Id)//dısardan alıyo bu ıd yı ona gore sılme ıslemı yapıyo
        {
            Category category = db.Categories.FirstOrDefault(x => x.ID == Id);//dısardan aldıgı ıdyı veri tabanıyla esıtse
            category.IsDeleted = true;
            category.DeleteDate = DateTime.Now;//sılınme tarıhınıde alsın dıyorum
            db.SaveChanges();

            return Json("");//olayı sılıp dısarıyo bos strıng ıfade yolluyo 
        }

    }
}