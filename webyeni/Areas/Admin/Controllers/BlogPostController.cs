﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webyeni.Areas.Admin.Models.DTO;
using webyeni.Areas.Admin.Models.Services.HTMLDataSourceServices;
using webyeni.Models.ORM.Entity;

namespace webyeni.Areas.Admin.Controllers
{
    public class BlogPostController : BaseController
    {

        public ActionResult Index()//bunları listelemem gerekıyo
        {
            List<BlogPostVM> model = db.BlogPosts.Where(x => x.IsDeleted == false).OrderBy(x => x.AddDate).Select(x => new BlogPostVM()
            {
                Title = x.Title,
                CategoryName = x.category.Name,//yukarda sorguyu yazdım sonra lıstelettım
                ID = x.ID
            }).ToList();

            return View(model);
        }


        public ActionResult AddBlogPost()
        {
            BlogPostVM model = new BlogPostVM();
            model.drpCategories = DrpServices.getDrpCategories();
            return View(model);//olusturdugum kontrolu vıewın ıcınde verileri dondur dıyom
        }

     
        //crk editor icin html algılamaması ıcın ayarlarıyla oynıcam
        [ValidateInput(false)]
        [HttpPost]   //tıklatdıktan sonra veri muhabbetleri
        public ActionResult AddBlogPost(BlogPostVM model)
        {
            BlogPostVM vmodel = new BlogPostVM();
            vmodel.drpCategories = DrpServices.getDrpCategories();
            //if (ModelState.IsValid)
            //{
            //    foreach(string name in Request.Files)//butun resimleri bir bir alsın dıyorum
            //    {
            //        model.PostImage = Request.Files[name];
            //        model.PostImage = SaveAs(Server.MapPath(" ~/Areas/Admin/Content/Site/images/blogpost/"+model.PostImage.FileName));
            //    }
            if (ModelState.IsValid)
            {
                string filename = "";
                foreach (string name in Request.Files)//butun resimleri bir bir alsın dıyorum
                {

                    model.PostImage = Request.Files[name];
                    string ext = Path.GetExtension(Request.Files[name].FileName);
                    if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")//dosyanın uzantılarını koyuyuorum
                    {
                        string uniqnumber = Guid.NewGuid().ToString();//yeni isim alması ve strınge donustursun dıyom
                        filename = uniqnumber + model.PostImage.FileName;//aynı olmasın diye
                        model.PostImage.SaveAs(Server.MapPath("~/Areas/Admin/Content/Site/images/blogpost/" + filename));
                    }

                }

                //her foreacte bi insert kullanılacak
                BlogPost blogpost = new BlogPost();//burada insert islemi yapacagım icin modelden veritabanına atıyorumn
                blogpost.CategoryID = model.CategoryID;
                blogpost.Title = model.Title;
                blogpost.content = model.Content;
                blogpost.ImagePath =filename;//bunuda database atıyorum //isimleri degistiriyorum ole unik olarak basıyorum veritabanına

                db.BlogPosts.Add(blogpost);
                db.SaveChanges();
                ViewBag.IslemDurum = 1;
                return View(vmodel);
            }
            else
            {
                ViewBag.IslemDurum = 2;
                return View(vmodel);
            }
            }

        public ActionResult UpdateBlogPost(int id)//idyi dısardan girdiricem
  {
            //    BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == id);
            //    BlogPostVM model = new BlogPostVM();//yukarda blogpost modelını yakaladım


            //    //simdi icerigi doldurucam
            //    model.CategoryID = blogpost.CategoryID;
            //    model.Title = blogpost.Title;
            //    model.Content = blogpost.content;
            //    model.drpCategories = DrpServices.getDrpCategories();//burda yazdıggım sevice  geri cagırıyorum

            //    return View(model);

            BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == id);
            BlogPostVM model = new BlogPostVM();
            model.CategoryID = blogpost.CategoryID;
            model.Title = blogpost.Title;
            model.Content = blogpost.content;
            model.drpCategories = DrpServices.getDrpCategories();

            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UpdateBlogPost(BlogPostVM model)
        {
            model.drpCategories = DrpServices.getDrpCategories();
            if (ModelState.IsValid)
            {
                BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == model.ID);
                blogpost.CategoryID = model.CategoryID;
                blogpost.Title = model.Title;
                blogpost.content = model.Content;

                db.SaveChanges();
                ViewBag.IslemDurum = 1;

                return View(model);
            }
            else
            {
                ViewBag.IslemDurum = 2;
                return View(model);
            }

        }


        public JsonResult DeleteBlogPost(int id)
        {
            BlogPost blogpost = db.BlogPosts.FirstOrDefault(x => x.ID == id);
            blogpost.IsDeleted = true;
            blogpost.DeleteDate = DateTime.Now;
            db.SaveChanges();
            return Json("");

        }

    }
}