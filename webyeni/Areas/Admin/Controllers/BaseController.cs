﻿using webyeni.Models.ORM.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace webyeni.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {

        //bundan bitane olusturucam digerleri bundan kalıtım alacak

        public BlogContext db;//direk burda veri tabanını cagirdık kurucu metodla db yazdıgımda ondan dırek veritabanını  cagrıyo

        public BaseController()//kurucu metod her calıstıgında bunu guncellesın dıyom
        {
          //  ViewBag.User = HttpContext.User.Identity.Name;//burası sorunlu ısımı  vermıyo
            db = new BlogContext();
        }

        protected override void Dispose(bool disposing)//duzenliyorum
        {
          db.Dispose();
        }
    }
}